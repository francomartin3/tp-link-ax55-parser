FROM cypress/included:10.7.0
COPY . /cypress
WORKDIR /cypress
USER 0
CMD ["cypress","run"]