describe('Scrapper', () => {
  it('scrape', () => {
    let devices = []
    const today = new Date();
    const yyyy = today.getFullYear();
    let mm = today.getMonth() + 1; // Months start at 0!
    const formattedToday = yyyy + '-' + mm;
    cy.visit(Cypress.env('ROUTER_IP'))
    cy.get('input[type="password"]').first().type(Cypress.env('ROUTER_PASSWORD'), { log: false })

    cy.get('a[title="LOG IN"]').first().click()

    // If there is another user logged in
    cy.wait(2000)
    cy.get('body').then($body => {
      if ($body.find('#user-conflict-prompt').length) {
        cy.get('#user-conflict-prompt').within(()=>{
          cy.get('a[title="LOG IN"]').click()
        })
      }
    })

    // Click on clients
    cy.get('[id="map-clients"]').first().click()
    cy.log("clicked on map clients")
    cy.intercept({
      method: 'POST',
      url: 'cgi-bin/luci/*/admin/smart_network**',
    }).as('getClients')
    cy.wait("@getClients")
    cy.get("[id='networkmap-client-panel-title']", {timeout: 10000})
    // Loop on table
    //cy.get('table').find('td').each(($el, $index) => {
    cy.get('.grid-content-tr').each(($element)=>{
      //cy.wrap($element).invoke('attr','devicename').then(deviceName => device['name'] = deviceName)
      //let id = cy.wrap($element).invoke('attr','id')
      //let id = $element.id()
      let id = $element[0].id
      if(id !== ""){
        let device = {}
        cy.get(`#${id}`).within($el => {
          //cy.get('.phone').invoke('attr','devicename').then(deviceName => device['name'] = deviceName)
          cy.get(".name").invoke('text').then(name => device['name'] = name)
          cy.get(".mac").first().invoke('text').then(mac => device['mac'] = mac)
          cy.get(".ip").first().invoke('text').then(ip => device['ip'] = ip)
          cy.get(".speed-upload-container").within(() =>{

            let speedNumber = 0.69
            cy.get(".text").invoke('text').then(speedAsText =>{speedNumber= parseFloat(speedAsText)})
            cy.get(".unit").invoke('text').then(unitAsText =>{
              if(unitAsText === "Kb/s")
                speedNumber = speedNumber*1000
              if(unitAsText === "Mb/s")
                speedNumber = speedNumber*1000*1000
              if(unitAsText === "Gb/s")
                speedNumber = speedNumber*1000*1000*1000
              device['uploadSpeedBytes']= speedNumber
            })
          })
          cy.get(".speed-download-container").within($speed =>{

            let speedNumber = 0.69
            cy.get(".text").invoke('text').then(speedAsText =>{speedNumber= parseFloat(speedAsText)})
            cy.get(".unit").invoke('text').then(unitAsText =>{
              if(unitAsText === "Kb/s")
                speedNumber = speedNumber*1000
              if(unitAsText === "Mb/s")
                speedNumber = speedNumber*1000*1000
              if(unitAsText === "Gb/s")
                speedNumber = speedNumber*1000*1000*1000
              device['downloadSpeedBytes']= speedNumber
            })
          })
         cy.get(".grid-content-td-deviceTag").invoke('text').then(tx=>{
          if(tx === "Interface:5G-1")
            device['interface']= "WIFI 5Ghz"
          if(tx === "Interface:2.4G")
            device['interface']= "WIFI 2.4Ghz"
          if(tx === "Interface:")
            device['interface']= "Wired"
        })
          device['@timestamp'] = new Date().toISOString()
          cy.request({
            method:'POST',
            url: `${Cypress.env('OS_URL')}tplink-scrapper-${formattedToday}/_doc`,
            auth: {user: "tplink-scrapper", password: Cypress.env("OS_PWD")},
            body: device,
            headers: {
              "content-type": "application/json"
            }
          })
          //devices.push(device)
      })
    }
    })
    cy.get('a[title="Log Out"]').first().click()
    cy.get('a[title="LOG OUT"]').first().click()
    //cy.writeFile('./data/output.json', devices)
  })
})